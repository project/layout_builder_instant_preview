# Layout Builder Instant Preview

The Layout Builder Instant Preview module provides live previews when editing
custom blocks in Layout Builder.

This module is a fork of [Panopoly Magic](https://www.drupal.org/project/panopoly_magic). The differences between the modules are:

- Panopoly Magic displays block forms in a modal dialog, while this module uses
the default off-canvas sidebar.
- This module only provides instant previews for custom blocks, not other fields
or system blocks.
- This module provides instant previews when configuring a layout section.
- This module adds support for Drupal 10 and CKEditor 5.
