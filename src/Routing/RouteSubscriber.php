<?php

namespace Drupal\layout_builder_instant_preview\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Layout Builder Instant Preview route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Replace the layout builder add block route with our own.
    if ($route = $collection->get('layout_builder.add_block')) {
      $route->setDefaults([
        '_controller' => '\Drupal\layout_builder_instant_preview\Controller\LayoutBuilderAddBlockController::addBlock',
        '_title' => $route->getDefault('_title'),
      ]);
    }

    // Replace the layout builder update block form with our own.
    if ($route = $collection->get('layout_builder.update_block')) {
      $route->setDefault('_form', '\Drupal\layout_builder_instant_preview\Form\LayoutBuilderUpdateBlockForm');
    }

    // Replace the layout builder configure section form with our own.
    if ($route = $collection->get('layout_builder.configure_section')) {
      $route->setDefault('_form', '\Drupal\layout_builder_instant_preview\Form\LayoutBuilderConfigureSectionForm');
    }
  }

}
