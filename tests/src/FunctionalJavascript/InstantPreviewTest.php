<?php

namespace Drupal\Tests\layout_builder_instant_preview\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\ckeditor5\Traits\CKEditor5TestTrait;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Layout Builder Instant Preview tests.
 *
 * @group layout_builder_instant_preview
 */
class InstantPreviewTest extends WebDriverTestBase {

  use CKEditor5TestTrait;
  use ContextualLinkClickTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'layout_builder',
    'block',
    'node',
    'contextual',
    'field_ui',
    'ckeditor5',
    'media_library',
    'layout_builder_instant_preview',
    'layout_builder_instant_preview_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Enable layout builder and overrides.
    $this->drupalGet('admin/structure/types/manage/layout_builder_page/display/default');
    $this->submitForm([
      'layout[enabled]' => TRUE,
      'layout[allow_custom]' => TRUE,
    ], 'Save');
  }

  /**
   * Tests instant previews using a simple text input.
   */
  public function testPreviewTextInput() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill a simple text field and check instant preview works.
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title');
    $this->waitForTextInElement('basic block title', 'main');

    // Check that focus was not stolen from the title input.
    $title_input_has_focus = $this->getSession()->evaluateScript('return document.activeElement?.name == "settings[label]";');
    $this->assertTrue($title_input_has_focus, 'Title input is not focused');

    // Check instant preview is not triggered on every keypress when typing
    // quickly.
    $counter = $assert_session->elementExists('css', '#preview-counter');
    $initial_preview_count = (int) $counter->getText();
    $this->assertGreaterThan(0, $initial_preview_count, 'Preview count is empty or zero');
    $title_input->setValue('t');
    $title_input->setValue('ty');
    $title_input->setValue('typ');
    $title_input->setValue('typi');
    $title_input->setValue('typin');
    $title_input->setValue('typing');
    $title_input->setValue('typing ');
    $title_input->setValue('typing t');
    $title_input->setValue('typing te');
    $title_input->setValue('typing tex');
    $title_input->setValue('typing text');
    $this->waitForTextInElement('typing text', 'main');
    $final_preview_count = (int) $counter->getText();
    $this->assertGreaterThan($initial_preview_count, $final_preview_count, 'Preview counter has not changed');
    // The final preview count should equal the initial count + 1, but allow for
    // a small margin of error.
    $this->assertLessThanOrEqual($initial_preview_count + 2, $final_preview_count, 'Preview counter has increased too much. Preview is likely being triggered on every keypress.');
  }

  /**
   * Tests instant previews using CKEditor 5.
   */
  public function testPreviewCKEditor() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill CKEditor and check instant preview works.
    $this->waitForEditor();
    $ckeditor = $assert_session->elementExists('css', 'div.ck-content');
    // Wait for our setInterval() hack to work before filling the editor.
    sleep(2);
    $ckeditor->setValue('basic block body');
    $this->waitForTextInElement('basic block body', 'main');

    // Check that focus was not stolen from CKEditor.
    $title_input_has_focus = $this->getSession()->evaluateScript('return document.activeElement?.classList?.contains("ck-content");');
    $this->assertTrue($title_input_has_focus, 'CKEditor is not focused');

    // Check instant preview is not triggered on every keypress when typing
    // quickly.
    $counter = $assert_session->elementExists('css', '#preview-counter');
    $initial_preview_count = (int) $counter->getText();
    $this->assertGreaterThan(0, $initial_preview_count, 'Preview count is empty or zero');
    // setValue() appends text to CKEditor instead of replacing it for some
    // reason, but it doesn't matter for the purposes of this test.
    $ckeditor->click();
    $ckeditor->setValue('t');
    $ckeditor->setValue('ty');
    $ckeditor->setValue('typ');
    $ckeditor->setValue('typi');
    $ckeditor->setValue('typin');
    $ckeditor->setValue('typing');
    $ckeditor->setValue('typing ');
    $ckeditor->setValue('typing t');
    $ckeditor->setValue('typing te');
    $ckeditor->setValue('typing tex');
    $ckeditor->setValue('typing text');
    $this->waitForTextInElement('typing text', 'main');
    $final_preview_count = (int) $counter->getText();
    $this->assertGreaterThan($initial_preview_count, $final_preview_count, 'Preview counter has not changed');
    // The final preview count should equal the initial count + 1, but allow for
    // a small margin of error.
    $this->assertLessThanOrEqual($initial_preview_count + 2, $final_preview_count, 'Preview counter has increased too much. Preview is likely being triggered on every keypress.');
  }

  /**
   * Tests instant previews using the Media Library widget.
   */
  public function testPreviewMediaLibrary() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Block with media')->click();

    // Upload media.
    $test_files = $this->getTestFiles('image');
    $image_0_path = $this->container->get('file_system')->realpath($test_files[0]->uri);
    $image_1_path = $this->container->get('file_system')->realpath($test_files[1]->uri);
    $this->uploadImage($image_0_path, 'image 0');
    $this->assertNotEmpty($assert_session->waitForElement('css', 'main img[alt="image 0"]'));
    $this->uploadImage($image_1_path, 'image 1');
    $this->assertNotEmpty($assert_session->waitForElement('css', 'main img[alt="image 1"]'));

    // Remove media.
    $this->click('.js-media-library-widget input[value="Remove"]');
    $this->assertTrue($assert_session->waitForElementRemoved('css', 'main img[alt="image 0"]'));
    $assert_session->elementExists('css', 'main img[alt="image 1"]');

    // Check that the preview event is not fired multiple times when editing
    // text after using the media library.
    $counter = $assert_session->elementExists('css', '#preview-counter');
    $initial_preview_count = (int) $counter->getText();
    $this->assertGreaterThan(0, $initial_preview_count, 'Preview count is empty or zero');
    $title_input = $assert_session->fieldExists('settings[label]');
    $title_input->setValue('t');
    $title_input->setValue('ty');
    $title_input->setValue('typ');
    $title_input->setValue('typi');
    $title_input->setValue('typin');
    $title_input->setValue('typing');
    $title_input->setValue('typing ');
    $title_input->setValue('typing t');
    $title_input->setValue('typing te');
    $title_input->setValue('typing tex');
    $title_input->setValue('typing text');
    $this->waitForTextInElement('typing text', 'main');
    $final_preview_count = (int) $counter->getText();
    $this->assertGreaterThan($initial_preview_count, $final_preview_count, 'Preview counter has not changed');
    // The final preview count should equal the initial count + 1, but allow for
    // a small margin of error.
    $this->assertLessThanOrEqual($initial_preview_count + 2, $final_preview_count, 'Preview counter has increased too much. Preview is likely being triggered on every keypress.');

    // Check that the preview is not triggering in an infinite loop.
    sleep(3);
    $final_preview_count_after_sleep = (int) $counter->getText();
    // Again, allow for a small margin of error.
    $this->assertLessThanOrEqual($final_preview_count + 1, $final_preview_count_after_sleep, 'Preview counter is increasing with no user input. Possible infinite loop.');
  }

  /**
   * Tests adding a new block, then canceling.
   */
  public function testCancelAddBlockForm() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill the title field.
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 1');
    $this->waitForTextInElement('basic block title 1', 'main');
    $assert_session->elementsCount('css', '.block-inline-blockbasic', 1);
    sleep(1);

    // Cancel the block form using the Cancel button and check the block preview
    // disappears.
    $this->click('.layout-builder-update-block input[data-drupal-selector="edit-actions-cancel"]');
    $this->assertTrue($assert_session->waitForElementRemoved('css', '.block-inline-blockbasic'));
    $assert_session->pageTextNotContains('basic block title 1');
    sleep(1);

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill the title field.
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 2');
    $this->waitForTextInElement('basic block title 2', 'main');
    $assert_session->elementsCount('css', '.block-inline-blockbasic', 1);
    sleep(1);

    // Cancel the block form using the X button and check the block preview
    // disappears.
    $this->click('.ui-dialog-titlebar-close');
    $this->assertTrue($assert_session->waitForElementRemoved('css', '.block-inline-blockbasic'));
    $assert_session->pageTextNotContains('basic block title 2');
    sleep(1);

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill the title field.
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 3');
    $this->waitForTextInElement('basic block title 3', 'main');
    $assert_session->elementsCount('css', '.block-inline-blockbasic', 1);

    // Cancel the block form by abandoning the Layout Builder.
    $this->drupalGet("node/$nid");
    $assert_session->pageTextNotContains('basic block title');
    $this->drupalGet("node/$nid/layout");
    $assert_session->pageTextNotContains('basic block title');
  }

  /**
   * Tests editing an existing block, then canceling.
   */
  public function testCancelEditBlockForm() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Add a basic block.
    $this->clickLink('Add block');
    $assert_session->waitForElement('css', '.inline-block-create-button')->click();
    $assert_session->waitForLink('Basic block')->click();

    // Fill the title and body fields.
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 1');
    $this->waitForEditor();
    $ckeditor = $assert_session->elementExists('css', 'div.ck-content');
    $ckeditor->setValue('basic block body 1');
    $this->waitForTextInElement('basic block title 1', 'main');
    $this->waitForTextInElement('basic block body 1', 'main');
    $assert_session->elementsCount('css', '.block-inline-blockbasic', 1);
    sleep(1);

    // Save the block and the layout.
    $this->click('.layout-builder-update-block input[data-drupal-selector="edit-actions-submit"]');
    $assert_session->waitForElementRemoved('css', '.layout-builder-update-block');
    $this->click('#edit-submit');
    $assert_session->pageTextContains('The layout override has been saved');
    $assert_session->pageTextContains('basic block title 1');
    $assert_session->pageTextContains('basic block body 1');
    $assert_session->elementNotExists('css', '#layout-builder');

    // Edit the block again.
    $this->drupalGet("node/$nid/layout");
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 2');
    $this->waitForTextInElement('basic block title 2', 'main');
    sleep(1);

    // Cancel the block form using the Cancel button and check the block preview
    // reverts to its original value.
    $this->click('.layout-builder-update-block input[data-drupal-selector="edit-actions-cancel"]');
    $this->waitForTextInElement('basic block title 1', 'main');
    $this->click('#edit-submit');
    $assert_session->pageTextContains('The layout override has been saved');
    $assert_session->pageTextContains('basic block title 1');
    $assert_session->elementNotExists('css', '#layout-builder');

    // Edit the block again.
    $this->drupalGet("node/$nid/layout");
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $title_input = $assert_session->waitForField('settings[label]');
    $this->assertNotEmpty($title_input);
    $title_input->setValue('basic block title 3');
    $this->waitForTextInElement('basic block title 3', 'main');
    sleep(1);

    // Cancel the block form using the X button and check the block preview
    // reverts to its original value.
    $this->click('.ui-dialog-titlebar-close');
    $this->waitForTextInElement('basic block title 1', 'main');
    $this->click('#edit-submit');
    $assert_session->pageTextContains('The layout override has been saved');
    $assert_session->pageTextContains('basic block title 1');
    $assert_session->elementNotExists('css', '#layout-builder');
  }

  /**
   * Tests editing a layout section.
   */
  public function testEditSection() {
    $rand = $this->randomMachineName();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();

    // Create a page and edit the layout.
    $nid = $this->createNode([
      'type' => 'layout_builder_page',
      'title' => "node $rand",
    ])->id();
    $this->drupalGet("node/$nid/layout");

    // Check this module doesn't interfere with adding a new section.
    $this->clickLink('Add section');
    $assert_session->waitForLink('Two column')->click();
    $assert_session->waitForField('layout_settings[label]')->setValue("section $rand");
    // Previewing new sections is not currently supported. Check the buttons are
    // not present.
    $assert_session->elementNotExists('css', 'input[data-drupal-selector="edit-actions-cancel"]');
    $assert_session->elementNotExists('css', 'input[data-drupal-selector="edit-actions-preview"]');
    $this->click('.layout-builder-configure-section input[data-drupal-selector="edit-actions-submit"]');
    $assert_session->waitForElementRemoved('css', '.layout-builder-configure-section');
    $assert_session->waitForText("Configure section $rand");

    // Edit the section.
    $this->clickLink("Configure section $rand");
    $assert_session->waitForField('layout_settings[column_widths]')->setValue('33-67');
    $assert_session->waitForElement('css', '.layout--twocol-section--33-67');
    $assert_session->waitForField('layout_settings[column_widths]')->setValue('75-25');
    $assert_session->waitForElement('css', '.layout--twocol-section--75-25');
    $assert_session->elementExists('css', 'input[data-drupal-selector="edit-actions-cancel"]');
    $assert_session->elementExists('css', 'input[data-drupal-selector="edit-actions-preview"]');

    // Save the section and layout.
    $this->click('.layout-builder-configure-section input[data-drupal-selector="edit-actions-submit"]');
    $assert_session->waitForElementRemoved('css', '.layout-builder-configure-section');
    $this->click('#edit-submit');
    $assert_session->pageTextContains('The layout override has been saved');
    $assert_session->elementExists('css', '.layout--twocol-section--75-25');

    // Cancel the section form using the X button and check the section preview
    // reverts to its original value.
    $this->drupalGet("node/$nid/layout");
    $this->clickLink("Configure section $rand");
    $assert_session->waitForField('layout_settings[column_widths]')->setValue('25-75');
    $assert_session->waitForElement('css', '.layout--twocol-section--25-75');
    $this->click('.ui-dialog-titlebar-close');
    $assert_session->waitForElement('css', '.layout--twocol-section--75-25');
  }

  /**
   * Waits for text within an element.
   *
   * @param string $text
   *   Text that must appear within the element.
   * @param string $selector
   *   CSS selector of the element to search.
   * @param int $timeout
   *   Maximum wait time in seconds.
   */
  protected function waitForTextInElement($text, $selector, $timeout = 5) {
    $element = $this->getSession()->getPage()->find('css', $selector);
    $this->assertNotEmpty($element);
    $result = $element->waitFor($timeout, function ($element) use ($text) {
      return strpos($element->getText(), $text) !== FALSE;
    });
    $this->assertNotEmpty($result, "\"$text\" not found");
  }

  /**
   * Uploads and creates a new image media item.
   *
   * @param string $image_path
   *   Path of the image to upload.
   * @param string $alt_text
   *   Image alt text.
   */
  protected function uploadImage($image_path, $alt_text) {
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $add_media_selector = '.js-media-library-open-button';

    $add_media_button = $assert_session->waitForElement('css', $add_media_selector);
    $this->assertNotEmpty($add_media_button);
    $this->click($add_media_selector);
    $file_field = $assert_session->waitForField('Add files');
    $this->assertNotEmpty($file_field);
    sleep(5);
    $file_field->attachFile($image_path);
    $assert_session->waitForField('Alternative text')->setValue($alt_text);
    $buttons = $assert_session->elementExists('css', '.ui-dialog-buttonpane');
    $buttons->pressButton('Save');
    $this->waitForTextInElement('Insert selected', '.ui-dialog-buttonpane');
    $assert_session->elementExists('css', '.ui-dialog-buttonpane')->pressButton('Insert selected');
    $assert_session->waitForElement('css', '.js-media-library-widget input[value="Remove"]');
  }

}
