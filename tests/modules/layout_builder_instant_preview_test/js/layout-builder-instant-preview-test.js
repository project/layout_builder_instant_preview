/**
 * @file
 * Adds a counter to test how many times the preview has been triggered.
 */

(function (Drupal, once, $) {

  Drupal.behaviors.layoutBuilderInstantPreviewTest = {

    attach: function (context) {
      if (!document.getElementById('preview-counter')) {
        document.querySelector('main').insertAdjacentHTML('beforeend', '<div id="preview-counter">0</div>');
      }

      once('test-log-preview', '.layout-builder-instant-preview', context).forEach((button) => {
        $(button).on('mousedown', (e) => {
          var count = parseInt(document.getElementById('preview-counter').innerHTML);
          document.querySelector('#preview-counter').innerHTML = count + 1;
        });
      });
    }

  }

})(Drupal, once, jQuery);
