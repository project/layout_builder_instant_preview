/**
 * @file
 * Triggers the preview button when values in the custom block form are changed.
 */

(function ($, Drupal) {

  // Holds the un-monkey-patched version of Drupal.behaviors.dialog.
  let drupalBehaviorsDialogAttach = null;

  // Used to as a "kill switch" to prevent the dialog from stealing focus on preview reload.
  let preventDialogStealingFocus = false;

  // Whether we have attached event listeners to the CKEDITOR object yet.
  let ckeditor4Bound = false;

  const form_selector = 'form#layout-builder-update-block, form#layout-builder-configure-section';

  // Submits the form.
  function submitForm() {
    // Skip preview when CKEditor 4 is maximized. The preview isn't visible
    // anyway, and it sometimes causes weird styling issues.
    if (document.getElementsByClassName('cke_maximized').length) {
      return;
    }

    const $preview = $('.layout-builder-instant-preview');

    // Disable the preview button.
    $preview.prop('disabled', true);

    // Prevent dialog.ajax.js from stealing focus.
    preventDialogStealingFocus = true;

    // Note: .click does not work here.
    // See https://drupal.stackexchange.com/questions/11638/how-to-programmatically-trigger-a-click-on-an-ajax-enabled-form-submit-button
    $preview.mousedown();
  };

  // Cancels the form.
  function triggerCancel(form_selector, e) {
    var cancel = $(form_selector).find('[data-drupal-selector="edit-actions-cancel"]');
    if (cancel.length) {
      cancel.trigger('mousedown');
      e.preventDefault();
      return false;
    }
  };

  // Core's formUpdated event is already debounced by 300ms. Add a small
  // additional debounce, since this event tends to be fired multiple times
  // very quickly after using the Media Library widget.
  debounceSubmitForm = Drupal.debounce(submitForm, 200);
  // A slower debounce for CKEditor 5 to match the above total time.
  debounceSubmitFormCKEditor5 = Drupal.debounce(submitForm, 500);

  Drupal.behaviors.layoutBuilderInstantPreview = {

    attach: function (context) {
      // Listen for core's formUpdated event to tell us when a form element has
      // changed.
      once('layoutBuilderInstantPreview', form_selector).forEach((form) => {
        $(form).on('formUpdated', function (e) {
          if (
            // Do not submit if one of the submit buttons triggered the event to
            // avoid an infinite loop.
            e.target.name != 'op'
            // Do not submit if the form itself triggered the event. After the
            // Media Library widget is used, this event is fired frequently on
            // the form itself for some reason, when any form field is changed.
            && e.target.tagName != 'FORM'
            // Ignore the Media Library widget. It is handled separately below.
            && !e.target.closest('.js-media-library-widget')
            // Do not submit when editing a link field. Validation errors may
            // occur, and there's no visual change when changing a link anyway.
            && !e.target.name?.endsWith('[uri]')
            // Ignore CKEditor 5. It is handled separately below.
            && !(e.target.getAttribute('data-ckeditor5-id') || e.target.classList.contains('ck-content') || e.target.closest('.ck-source-editing-area') != null))
          {
            debounceSubmitForm();
          }
        });

        // As of Drupal 10.2.5, the core debounce function used to trigger the
        // formUpdated event for CKEditor 5 has been changed to fire the event
        // immediately, causing the last few keystrokes to not be included in
        // the preview when typing quickly. So, listen to the editor's
        // change:data event instead.
        //
        // There doesn't seem to be an event to notify us when a CKEditor 5
        // instance is created... so we have to make do by polling until the
        // editor is created.
        once('instant-preview-editor', '[data-ckeditor5-id]', form).forEach((textarea) => {
          const interval = setInterval(() => {
            const editor = Drupal.CKEditor5Instances.get(textarea.getAttribute('data-ckeditor5-id'));
            if (editor) {
              editor.model.document.on('change:data', debounceSubmitFormCKEditor5);
              clearInterval(interval);
            }
          }, 500);
        });

        // Trigger the "Cancel" button when the dialog is closed in other ways
        // so the layout always gets reverted correctly.
        if ($(form).find('[data-drupal-selector="edit-actions-cancel"]').length) {
          // When the Edit Block dialog is opened, the dialog close button
          // hasn't been created yet. Wait for it to appear before messing with
          // its events.
          const interval = setInterval(() => {
            const $closeButton = $(form)
              .closest('.ui-dialog-off-canvas')
              .find('.ui-dialog-titlebar-close');
            if ($closeButton.length) {
              // Trigger "Cancel" button when off canvas tray closed using the close
              // button.
              $closeButton
                .off('click')
                .click(function (e) {
                  return triggerCancel(form, e);
                });

              // Trigger "Cancel" button when off canvas tray closed using the ESC
              // key.
              $(form)
                .closest('.ui-dialog-off-canvas')
                .off('keydown')
                .keydown(function (e) {
                  if (e.keyCode === 27) {
                    return triggerCancel(form, e);
                  }
                });
              clearInterval(interval);
            }
          }, 500);
        }
      });

      // The formUpdated is not fired reliably for the Media Library widget.
      // Submit the form if the media library widget is reloaded after adding or
      // removing a media item, or editing a media item with media_library_edit.
      if (context.classList && (context.classList.contains('js-media-library-widget') || context.closest('.js-media-library-widget'))) {
        debounceSubmitForm();
      }

      // When the preview is rendered, misc/dialog/dialog.ajax.js will steal focus from whatever it was on. Here we
      // monkey-patch Drupal.behaviors.dialog.attach to give ourselves a "kill switch" for when the preview is
      // rendering, that is the preventDialogStealFocus variable.
      if (!drupalBehaviorsDialogAttach && Drupal.behaviors.dialog) {
        drupalBehaviorsDialogAttach = Drupal.behaviors.dialog.attach;
        Drupal.behaviors.dialog.attach = function (context, settings) {
          if (preventDialogStealingFocus && $(context).closest('.ui-dialog-off-canvas').length > 0) {
            preventDialogStealingFocus = false;
          }
          else {
            drupalBehaviorsDialogAttach(context, settings);
          }
        };
      }

      // Instant previews are suppressed when CKEditor 4 is maximized. When the
      // user un-maximizes the editor, update the preview.
      if (typeof CKEDITOR !== 'undefined' && !ckeditor4Bound) {
        ckeditor4Bound = true;
        CKEDITOR.on('instanceCreated', function(e) {
          e.editor.on('maximize', function (e) {
            if (e.data == 2) {
              submitForm();
            }
          });
        });
      }

      // Preserve the active block highlight style when the preview is rendered.
      let highlightId = $('form[data-layout-builder-target-highlight-id]')
        .attr('data-layout-builder-target-highlight-id');
      if (highlightId) {
        $('.is-layout-builder-highlighted').removeClass(
          'is-layout-builder-highlighted',
        );
        $(`[data-layout-builder-highlight-id="${highlightId}"]`).addClass(
          'is-layout-builder-highlighted',
        );
      }
    },

  }

})(jQuery, Drupal);
